#!bin/python
import time
from selenium import webdriver

driver_path = "/opt/homebrew/bin/chromedriver"
brave_path = '/Applications/Brave Browser.app/Contents/MacOS/Brave Browser'

option = webdriver.ChromeOptions()
option.binary_location = brave_path

driver = webdriver.Chrome(executable_path=driver_path, options=option)
# Create new Instance of Chrome
driver.get("http://10.10.11.2:5000/home")
time.sleep(5)
while True:
	driver.refresh()
	time.sleep(5)
